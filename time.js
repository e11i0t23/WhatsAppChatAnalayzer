var tc = require("timezonecomplete");

module.exports = {
  "count": function count(arr){
    time = 0
    longestTime = [0,0]
    longestMSGs = [0,0]
    count = 0
    messageCount=0
    for(i=0; i<arr.length-1; i++){
      date1Arr = arr[i][0].split("/")
      date1 = date1Arr[2]+"-"+date1Arr[1]+"-"+date1Arr[0]
      time1 = arr[i][1]+":00"
      var start = new tc.DateTime(`${date1}T${time1} UTC`)

      date2Arr = arr[i+1][0].split("/")
      date2 = date2Arr[2]+"-"+date1Arr[1]+"-"+date1Arr[0]
      time2 = arr[i+1][1]+":00"
      var end = new tc.DateTime(`${date2}T${time2} UTC`)
      
      var duration = end.diff(start);  // unit-aware duration

      // console.log(duration.minutes()); // -120
      // console.log(duration.hours()); // -2
      // break

      if (duration.minutes()<5){
        count+=duration.minutes()
        messageCount++
        //console.log(time1)
      }else{
        if (longestTime[0]<duration.minutes() & messageCount > 1){
          longestTime=[duration.minutes(),messageCount]
        }
        if (longestMSGs[1]<messageCount & messageCount > 1){
          longestMSGs=[duration.minutes(),messageCount]
        }
        if (messageCount > 1){
          time = time + duration.minutes()
        }
        messageCount = 0
      }
    }

    date1Arr = arr[1][0].split("/")
    date1 = date1Arr[2]+"-"+date1Arr[1]+"-"+date1Arr[0]
    time1 = arr[1][1]+":00"
    var start = new tc.DateTime(`${date1}T${time1} UTC`)

    date2Arr = arr[arr.length-1][0].split("/")
    date2 = date2Arr[2]+"-"+date1Arr[1]+"-"+date1Arr[0]
    time2 = arr[arr.length-1][1]+":00"
    var end = new tc.DateTime(`${date2}T${time2} UTC`)
    
    var uptime = end.diff(start).toIsoString()

    return[time,longestTime,longestMSGs,uptime]   
  }
}