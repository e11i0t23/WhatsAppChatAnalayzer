# WhatsApp Chat Analyzer

A small program that lets you find out various information about your WhatsApp chats

Define your file by replacing "WhatsApp.txt" in index.js, line 7 with your chat export text file name.
Run by doing node index.js.

Please be aware this is a work in progress so may not work on all inputs and may provide inacurate data.