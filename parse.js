module.exports = {
    "parser": function parser(f){

        //takes input and parses in into an array based on DD/MM/YYYY
        let flist = f.split(/(?=^\d{1,2}\/\d{1,2}\/\d{4})/m).filter(Boolean)
        if(f.indexOf("You were added") >=0 & f.indexOf('created group "') >=0){
            flist.shift()
            flist.shift()
        }
        flist.shift() //removes opening message about encryption
        
        var users = []

        // loop through each message
        for(var i = 0;i<flist.length;i++){ 
            flist[i]=flist[i].replace(/\r?\n/g, ' ') // remove new line charecters
            
            
            //split to the 4 elements of a message (date, time, sender, message)
            a1=flist[i].split(/,(.+)/,2)
            a2=a1[1].split(/-(.+)/,2)
            a3=a2[1].split(/:(.+)/,2)

            date = a1[0].trim()
            time = a2[0].trim()
            user = a3[0].trim()
            message = a3[1].trim()
            console.log(a3)
            console.log(i)

            //insert into array
            flist[i]=[date,time,user,message]

            found = false
            for(var x=0;x<users.length;x++){
                if (users[x]==user){
                    found = true
                }
            }
            if (found == false){
                users.push(user)
            }
        }
        //return as arrays
        return[flist, users]
    }
}